package dvl.srg.transport;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import dvl.srg.datah.DataHandler;
import dvl.srg.model.Metadates;
import dvl.srg.utils.JsonFormaterHelper;
import dvl.srg.utils.ReadServerLocationDatesHelper;
import dvl.srg.utils.XmlFormaterHelper;

import java.io.*;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by administrator on 11/3/15.
 */
public class TransportCollectDates {

    private String jsonBFSList;
    private String jsonInputStream;
    private boolean isMaven;
    private Metadates serverMetadates;

    public TransportCollectDates (String jsonBFSList, String jsonInputStream, Metadates serverMetadates) {
        this.jsonBFSList = jsonBFSList;
        this.serverMetadates = serverMetadates;
        this.jsonInputStream = jsonInputStream;
    }

    public String getDates() throws IOException {

        if (jsonInputStream.isEmpty()) {
            isMaven = true;
            jsonInputStream = jsonBFSList;
        }

        String dates = getNodeDates();

        Map<String, Object> listBFS = getBFSJsonNodesMapObject();

        if (listBFS
                .get(serverMetadates
                        .getServerNr() + "").toString().isEmpty()) {
            return new DataHandler(isMaven).getFormatedDates(dates);
        }

        return new DataHandler(isMaven)
                .getFormatedDates(
                        getDatesFromFuture(
                                getArrayListFutureListDates(listBFS), dates));
    }

    private String getNodeDates() throws FileNotFoundException {

        return JsonFormaterHelper
                .toJson(XmlFormaterHelper
                        .fromXml(new File("./config/node" + serverMetadates.getServerNr() + ".xml")));
    }

    private  Map<String, Object>  getBFSJsonNodesMapObject() {
        return new Gson().fromJson(jsonInputStream,
                new TypeToken<Map<String, Object>>() {}.getType());
    }

    private Future<String>[] getArrayListFutureListDates(Map<String, Object> listBFS) {
        StringTokenizer stringTokenizer =
                new StringTokenizer(
                        listBFS.get(serverMetadates.getServerNr() + "").toString(), " ");

        ExecutorService service = Executors.newFixedThreadPool(stringTokenizer.countTokens());
        Future<String> [] datesFuture = new Future[stringTokenizer.countTokens()];
        int i = 0;

        while (stringTokenizer.hasMoreTokens()) {
            String nrRelNode = stringTokenizer.nextToken();
            System.out.println("[INFO] -----------------------------------------");
            System.out.println("GET DATA FROM NODE NR : " + nrRelNode);

            Metadates relationServerMetadates = ReadServerLocationDatesHelper.getModeMetadates(nrRelNode);
            datesFuture[i++] = service.submit(
                    new TransportClient(relationServerMetadates, jsonInputStream));
        }

        return datesFuture;
    }

    private String getDatesFromFuture(Future<String> [] datesFuture, String dates) {

        for (Future<String> listFuture: datesFuture) {
            try {
                dates = JsonFormaterHelper.mergeStreamDates(dates, listFuture.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        return dates.toString();
    }


}
