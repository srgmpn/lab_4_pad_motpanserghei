package dvl.srg.transport;

import dvl.srg.model.Metadates;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

import static org.apache.commons.lang3.SerializationUtils.serialize;

/**
 * Created by administrator on 10/12/15.
 */
public class TransportListener extends Thread {

    private Metadates serverMetadates;
    private boolean isStopped;
    private boolean isAccepted;
    private ServerSocket serverSocket;
    private String jsonBFSList;
    private String jsonInputStream;


    public TransportListener(Metadates serverMetadates, String jsonBFSList) {
        this.jsonBFSList = jsonBFSList;
        this.serverMetadates = serverMetadates;
        isStopped = false;
    }

    @Override
    public void run() {

        try {
            serverSocket = new ServerSocket(serverMetadates.getLocation().getPort());
            while (!isStopped) {
                Socket socket = serverSocket.accept();  // Blocking call!
                // You can use non-blocking approach
                isAccepted = true;
                readData(socket);
                writeData(socket);
                isAccepted = false;
            }
        } catch (SocketException e) {
            System.out.println("[WARNING] ----------------------------------------- \n" +
                    "[WARNING] Waiting time expired... Socket is closed.");
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void readData(Socket socket) throws IOException, ClassNotFoundException {
        jsonInputStream = (String) new ObjectInputStream(socket
                .getInputStream())
                .readObject();
    }

    private void writeData(Socket socket) throws IOException {
        String dates = new TransportCollectDates(jsonBFSList, jsonInputStream, serverMetadates).getDates();
        serialize(dates, socket.getOutputStream());
        socket.getOutputStream().flush();
        socket.close();
    }

    public void setStopped(boolean isStopped) {
        this.isStopped = isStopped;
        if (!isAccepted) {
            try {
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
