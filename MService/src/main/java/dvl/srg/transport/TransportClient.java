package dvl.srg.transport;

import dvl.srg.model.Metadates;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.concurrent.Callable;

import static org.apache.commons.lang3.SerializationUtils.deserialize;

/**
 * Created by administrator on 10/12/15.
 */
public class TransportClient implements Callable<String>{

    public final static String XSD_SCHEMA = "schema.xsd";
    public final static String MSG = "ERROR Validation";


    private Metadates metadates;
    private String jsonBFSList;

    public TransportClient() {
        jsonBFSList = "";
    }

    public TransportClient(Metadates metadates, String jsonBFSList) {
        this.metadates = metadates;
        this.jsonBFSList = jsonBFSList;
    }

    @Override
    public String call() throws Exception {
        return getEmployeesFrom(metadates, jsonBFSList);
    }

    public String getEmployeesFrom(Metadates metadates, String jsonBFSList) throws IOException {
        Socket socket = new Socket();
        socket.connect(metadates.getLocation());
        writeOutputStream(socket, jsonBFSList);
/*
        if (jsonBFSList.isEmpty()) {
            String stream = readInputData(socket);
            return stream;/*XmlFormaterHelper
                    .fromXml(new StringReader(stream)).toString();
                    /*( (XSDValidator.validateXMLSchema(XSD_SCHEMA, stream))?
                            XmlFormaterHelper
                                    .fromXml(new StringReader(stream)).toString() :
                    MSG);
        }
*/
        return readInputData(socket);
    }

    private void writeOutputStream(Socket socket, String jsonBFSList) throws IOException {
        ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
        outputStream.writeObject(jsonBFSList);
        outputStream.flush();
    }

    private String readInputData(Socket socket) throws IOException {
        String dates = (String) deserialize(socket.getInputStream());
        socket.close();
        return dates;
    }

}
