package dvl.srg.utils;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import dvl.srg.model.Employee;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by administrator on 10/27/15.
 */
public class JsonFormaterHelper {

    public static String mergeStreamDates (String dates, String inDates) {

        List<Employee> employees = fromJson(dates);
        employees.addAll(fromJson(inDates));

        return  toJson(employees);
    }


    public static String toJson(List<Employee> employees) {
        return new Gson().toJson(employees);
    }

    public static List<Employee> fromJson(String json) {
        Type type = new TypeToken<List<Employee>>(){}.getType();
        return new Gson().fromJson(json, type);
    }

}
