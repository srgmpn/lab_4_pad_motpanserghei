package dvl.srg.utils;

import dvl.srg.model.Employee;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
/**
 * Created by administrator on 10/27/15.
 */
public class XmlFormaterHelper {

    public static String toXml(List<Employee> employees) {

        StringWriter stringWriter = new StringWriter();;
        try{

            Element employeesElement = new Element("employees");
            Document doc = new Document(employeesElement);


            for (int i = 0; i < employees.size(); ++i) {

                Element employeeElement = new Element("employee");

                Element firstName = new Element("firstName");
                firstName.setText(employees.get(i).getFirstName());

                Element lastName = new Element("lastName");
                lastName.setText(employees.get(i).getLastName());

                Element department = new Element("department");
                department.setText(employees.get(i).getDepartment());

                Element salary = new Element("salary");
                salary.setText("" + employees.get(i).getSalary());


                doc.getRootElement().addContent(employeeElement
                                                        .addContent(firstName)
                                                        .addContent(lastName)
                                                        .addContent(department)
                                                        .addContent(salary));
            }

            XMLOutputter xmlOutput = new XMLOutputter();
            xmlOutput.setFormat(Format.getPrettyFormat());
            xmlOutput.output(doc, stringWriter);

        }catch(IOException e){
            e.printStackTrace();
        }
        return stringWriter.toString();
    }

    public static List<Employee> fromXml (File file) {

        try {
            SAXBuilder saxBuilder = new SAXBuilder();
            Document document = saxBuilder.build(file);

            return handle(document);

        } catch(JDOMException | IOException e){
            e.printStackTrace();
            return null;
        }

    }

    public static List<Employee> fromXml (StringReader stringReader) {

        try {
            SAXBuilder saxBuilder = new SAXBuilder();
            Document document = saxBuilder.build(stringReader);

            return handle(document);

        } catch(JDOMException | IOException e){
            e.printStackTrace();
            return null;
        }
    }

    private static List<Employee> handle(Document document) {
        List<Employee> employees = new ArrayList<>();
        Element rootElement = document.getRootElement();
        List<Element> emplList = rootElement.getChildren();

        for (int temp = 0; temp < emplList.size(); temp++) {
            Element employee = emplList.get(temp);

            String firstName = employee.getChild("firstName").getText();
            String lastName = employee.getChild("lastName").getText();
            String department = employee.getChild("department").getText();
            Double salary = Double.valueOf(employee.getChild("salary").getText());

            employees.add(new Employee(firstName, lastName, department, salary));
        }

        return employees;
    }
}
