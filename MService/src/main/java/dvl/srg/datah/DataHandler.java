package dvl.srg.datah;

import dvl.srg.model.Employee;
import dvl.srg.utils.JsonFormaterHelper;
import dvl.srg.utils.XmlFormaterHelper;

import java.util.List;

/**
 * Created by administrator on 10/27/15.
 */
public class DataHandler {

    private boolean isMaven;

    public DataHandler (boolean isMaven) {
        this.isMaven = isMaven;
    }

    public String getFormatedDates (String dates) {

        if (isMaven) {
            List<Employee> employees = JsonFormaterHelper.fromJson(dates);
            return XmlFormaterHelper.toXml(employees);
        }

        return dates;
    }
}
