package dvl.srg.datah;

import dvl.srg.model.Employee;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Created by administrator on 11/3/15.
 */
public class DataFormat {

    private List<Employee> employees;

    public DataFormat (List<Employee> employees) {
        this.employees = employees;
    }

    public List<Employee> showFiltered() {
        return Arrays.asList((Employee[]) employees.stream()
                .filter(e -> e.getSalary() > 6000.0)
                .sorted(Comparator.comparing(Employee::getLastName))
                .toArray());
    }

    public String getAverage () {
        return employees
                    .stream()
                    .mapToDouble((employee) -> employee.getSalary())
                    .average()
                    .toString();
    }
}
