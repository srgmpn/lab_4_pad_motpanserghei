package dvl.srg.main;


import dvl.srg.discovery.DiscoveryClient;
import dvl.srg.model.Employee;
import dvl.srg.model.Metadates;
import dvl.srg.transport.TransportClient;
import dvl.srg.utils.XmlFormaterHelper;
import dvl.srg.validator.XSDValidator;

import java.io.IOException;
import java.io.StringReader;
import java.net.InetSocketAddress;
import java.util.List;
import java.util.stream.Collectors;


public class App 
{
    public final static String XSD_SCHEMA = "schema.xsd";
    public final static String MSG = "Invalid Dates";

    public static void main( String[] args )
    {
        System.out.println("[INFO] -----------------------------------------\n" +
                "[INFO] Client is running...");

        try {
            Metadates nodeDates = new DiscoveryClient(
                    new InetSocketAddress("127.0.0.1", 30000))
                    .retrieveNodeDates();
            System.out.println("[INFO] -----------------------------------------\n" +
                    "[INFO] Discovered maven server: " + nodeDates);


            if (nodeDates != null) {
                String employeeLis =  new TransportClient()
                                                    .getEmployeesFrom(nodeDates, "");
/*                System.out.println("[INFO] -----------------------------------------");
                System.out.println("[INFO] Received invalidate dates ");
                System.out.println(employeeLis);
*/
                if (!XSDValidator.validateXMLSchema(XSD_SCHEMA, employeeLis)) {
                    System.out.println(MSG);
                    return;
                }

                List<Employee> employees = XmlFormaterHelper.fromXml(new StringReader(employeeLis));

                System.out.println("[Result] -----------------------------------------\n" +
                        "Discovered employees: " +
                        employees
                            .stream()
                            .collect(Collectors.groupingBy(Employee::getDepartment)));


                System.out.println("[SALARY AVERAGE] --------------------------------------");
                System.out.println("Average result: " + employees
                                                            .stream()
                                                            .mapToDouble((employee) -> employee.getSalary())
                                                            .average()
                                                    );

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
/*
    private List<Employee> showFiltered(List<Employee> list) {
        return Arrays.asList((Employee[]) list.stream()
                .filter(e -> e.getSalary() > 6000.0)
                .sorted(Comparator.comparing(Employee::getLastName))
                .toArray());
    }
*/
}
